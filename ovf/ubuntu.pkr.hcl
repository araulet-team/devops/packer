locals { 
  timestamp = regex_replace(timestamp(), "[- TZ:]", "") 
  vm_name = "${var.vm_basename}"
  vm_basename_folder = "${path.cwd}/builds/${local.vm_name}_v${var.vm_version}_${local.timestamp}"
}

# https://www.packer.io/docs/builders/virtualbox/iso
source "virtualbox-iso" "debian" {
  boot_wait    = "5s"
  boot_command = [
      " <wait>",
      " <wait>",
      " <wait>",
      " <wait>",
      " <wait>",
      "<esc><wait>",
      "<f6><wait>",
      "<esc><wait>",
      "<bs><bs><bs><bs><wait>",
      " autoinstall<wait5>",
      " ds=nocloud-net<wait5>",
      ";s=http://<wait5>{{.HTTPIP}}<wait5>:{{.HTTPPort}}/<wait5>",
      " ---<wait5>",
      "<enter><wait5>"
  ]
  disk_size            = "${var.disk_size}"
  http_directory       = "http"
  guest_os_type        = "Ubuntu_64"
  hard_drive_interface = "sata"
  headless             = "${var.headless}"
  iso_checksum         = "${var.iso_checksum}"
  iso_urls = [
    "${var.iso_url}"
  ]
  output_directory     = "${local.vm_basename_folder}"
  shutdown_command     = "echo '${var.ssh_username}' | sudo -S shutdown -P now"
  ssh_password         = "${var.ssh_password}"
  ssh_username         = "${var.ssh_username}"
  ssh_wait_timeout     = "10000s"
  ssh_handshake_attempts = "200"
  export_opts = [
    "--vsys", "0",
    "--manifest",
    "--description", "${var.description}",
    "--version", "${var.vm_version}"
  ]
  vboxmanage = [
    ["modifyvm", "{{ .Name }}", "--memory", "${var.memory}"],
    ["modifyvm", "{{ .Name }}", "--cpus", "${var.cpus}"],
    ["modifyvm", "{{ .Name }}", "--vram", "${var.ram}"],
    ["modifyvm", "{{ .Name }}", "--nictype1", "virtio"]
  ]
  guest_additions_path = "VBoxGuestAdditions_{{ .Version }}.iso"
  virtualbox_version_file = ".vbox_version"
  vm_name                 = "${local.vm_name}"
  gfx_efi_resolution      = "1920x1080"
}

# https://www.packer.io/docs/templates/hcl_templates/blocks/build
build {
  sources = ["source.virtualbox-iso.debian"]

  provisioner "shell" {
    environment_vars = [
      "http_proxy=${var.http_proxy}",
      "https_proxy=${var.https_proxy}",
      "ftp_proxy=${var.ftp_proxy}",
      "rsync_proxy=${var.rsync_proxy}",
      "no_proxy=${var.no_proxy}"
    ]
    execute_command = "echo '${var.ssh_username}' | {{ .Vars }} sudo -E -S sh '{{ .Path }}'"
    inline = [
      # https://github.com/phusion/baseimage-docker/issues/58
      "echo \"debconf debconf/frontend select Noninteractive\" | sudo debconf-set-selections",
      "echo Installing Git Ansible",
      "sleep 30",
      "echo ///// apt-get update /////",
      "sudo apt-get update",
      "echo ///// apt-get -y install git ansible /////",
      "sudo apt-get -y install git ansible",
      # https://www.continualintegration.com/miscellaneous-articles/how-do-you-solve-the-login-prompt-message-cloud-initmodulesfinaldatasource/
      "echo Clean up cloud-init",
      "sudo apt-get -y remove cloud-init",
      "echo \"debconf debconf/frontend select Dialog\" | debconf-set-selections"
    ]
  }
  
  provisioner "shell" {
    environment_vars = [
      "DESKTOP=${var.desktop}",
      "UPDATE=${var.update}",
      "INSTALL_VAGRANT_KEY=${var.install_vagrant_key}",
      "SSH_USERNAME=${var.ssh_username}",
      "SSH_PASSWORD=${var.ssh_password}",
      "http_proxy=${var.http_proxy}",
      "https_proxy=${var.https_proxy}",
      "ftp_proxy=${var.ftp_proxy}",
      "rsync_proxy=${var.rsync_proxy}",
      "no_proxy=${var.no_proxy}"
    ]
    execute_command   = "echo '${var.ssh_username}' | {{ .Vars }} sudo -E -S bash '{{ .Path }}'"
    expect_disconnect = "true"
    scripts = [
      "${path.cwd}/scripts/desktop.sh",
      "${path.cwd}/scripts/update.sh",
      "${path.cwd}/scripts/vagrant.sh",
      "${path.cwd}/scripts/virtualbox.sh",
      "${path.cwd}/scripts/cleanup.sh"
    ]
  }

}